<!doctype html>
<html>
  <head>
	<link rel="stylesheet" href="uts.css">
  
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>HOME</title>
	<style>

	</style>
	
	
  </head>
	<body>
		<div class="fix">
		<nav class="navbar navbar-expand-lg" style="background-color: darkblue;">
			<a class="navbar-brand text-white text-monospace font-italic font-weight-bold-" href="#">Tazkia Travel</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active">
						<a class="nav-link text-white" href="#">Home <span class="sr-only">(current)</span></a>
					</li>
				  
					<li class="nav-item">
						<a class="nav-link text-white" href="#">Haji</a>
					</li>
				  
					<li class="nav-item dropdown">
						<a class="nav-link text-white dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Paket Umroh
						</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
						  <a class="dropdown-item" href="#">Umroh Shafa</a>
						  <a class="dropdown-item" href="#">Umroh Marwah</a>
						  <a class="dropdown-item" href="#">Umroh Muhtazam</a>
						  <a class="dropdown-item" href="#">Umroh Ramadhan</a>
						</div>
					</li>
					
						<li class="nav-item dropdown">
						<a class="nav-link text-white dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						  Paket Umroh Plus
						</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
						  <a class="dropdown-item" href="#">Plus Dubai</a>
						  <a class="dropdown-item" href="#">Plus Turki</a>
						  <a class="dropdown-item" href="#">Plus Istanbul</a>
						</div>
					</li>
					
					<li class="nav-item ">
						<a class="nav-link text-white " href="#">Wisata Halal</a>
					</li>
					
					<li class="nav-item">
						<a class="nav-link text-white " href="#">Jadwal</a>
					</li>
					
					<li class="nav-item">
						<a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Tazkia Travel</a>
					</li>
				</ul>
				
				<form class="form-inline my-2 my-lg-0">
						<button class="btn btn-outline-success text-white " type="button">Hubungi Kami</button>
				</form>
			</div>
		</nav>
		</div>
		
		
		<div class="ya">
		<button type="button" class="btn btn-danger">Info Tazkia:</button>
		</div>
		
		<div class="bergerak">
				 <marquee scrollamount="5" color="red">|PENTING!  #PENUNDAAN KEBERANGKATAN UMROH  TAZKIA TRAVEL  UNTUK SEMENTARA WAKTU|          |PENTING!  #PENUNDAAN KEBERANGKATAN UMROH  TAZKIA TRAVEL  UNTUK SEMENTARA WAKTU|   PROMO SHAFA MILAD </marquee></a>
		</div>
		
		<div class="foto">
			<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
			  <ol class="carousel-indicators">
				<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
				<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
				<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
			  </ol>
			  <div class="carousel-inner">
				<div class="carousel-item active">
				  <img src="c.jpg" class="d-block w-100" alt="...">
				</div>
				<div class="carousel-item">
				  <img src="f.jpg" class="d-block w-100" alt="...">
				</div>
				<div class="carousel-item">
				  <img src="a.png" class="d-block w-100" alt="...">
				</div>
			  </div>
			  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			  </a>
			  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			  </a>
		</div>
		<div class="is">
			<h2>UMROH & HAJI KHUSUS  <font color="#FF6347" font="italic">Tazkia Travel</font></h2>
			<h5 font color="#FF6347" font="italic">Tazkia Travel</font> adalah Biro Perjalanan Umroh & Haji Plus Terbaik dan Terpercaya, InsyaAlloh kami siap memfasilitasi dan mendampingi Bapak Ibu untuk beribadah di tanah suci</h5>
		
		</div>
		
		<div class="haji">
			<div class="hajiisi">
				<h2>Pendaftaran  <font color="#DAA520" font="italic">Haji Plus</font></h2>
				<h6> Tazkia Tours & Travel Menerima Pendaftaran Haji Plus / Haji Khusus tahun 2020. <br> Adapun keberangkatan mengikuti nomor urut porsi Kementerian Agama Republik Indonesia
					<font color="#FFD700" font="italic">Daftar Sekarang</font> untuk mendapatkan nomor urut porsi haji lebih awal. <br> Jika, ada yang ditanyakan isi formulir disamping dan dapatlan info terbaru dari kami.</h6>		
			</div>
		</div>
		<div class="daftar">
			<div class="row">
				<div class="col-md-12">
					<div class="wrap">
						<p class="form-title">DAFTAR <font color="#DAA520" font="italic">Haji Plus</font></p>
						<p class="form-title2">Isikan data diri anda dan WhatsApp anda <br>maka kami akan menghubungi anda.</p>
						<form class="login">
						<input type="text" placeholder="Nama"/>
						<input type="text" placeholder="WhatsApp" />
						<div class="remember-forgot">
							<div class="row">
								<div class="col-md-6">
										<input type="submit" value="Login" class="btn btn-success btn-sm" />
									</div>
								</div>
								
							</div>
						</div>
						</form>
					
					</div>
				</div>
			</div>
		</div>
		
		<img src="b.png" class="d-block w-100" alt="...">
		
		
		<div class="card-deck">
		  <div class="card border-primary mb-3">
			<div class="card-body">
			  <h5 class="card-title">UMROH SHAFA <br> umroh 9 hari </h5>
			  <p class="card-text">$ 2.300 <br> Hotel Makkah *5 <br> Hotel Madinah *5 <br> Flight GA/ Saudia</p>
			</div>
			<div class="card-footer">
			  <small class="text-muted">Last updated 3 mins ago</small>
			</div>
		  </div>
		  <div class="card border-success mb-3">
			<div class="card-body">
			  <h5 class="card-title">UMROH MADINAH <br> umroh 9 hari </h5>
			  <p class="card-text">$ 1.950 <br> Hotel Makkah *5 <br> Hotel Madinah *5 <br> Flight GA/ Saudia</p>
			</div>
			<div class="card-footer">
			  <small class="text-muted">Last updated 3 mins ago</small>
			</div>
		  </div>
		  <div class="card border-danger mb-3">
			<div class="card-body">
			  <h5 class="card-title">LOGIN<br> Jika sudah ada akun </h5>
			  <form method="POST" action="uts.php">
				<table>
					<tr>
						<td>
							Nama :
						</td></td>
						<td>:</td>
						<td><input type="text" name="nma"></td> 
					</tr>
					
					<tr>
						<td>
							Password
						</td></td>
						<td>:</td>
						<td><input type="password" name="pw"></td> 
					</tr>
					
					<tr>
						<td>
							
						</td></td>
						<td></td>
						<td><input type="submit" name="kirim" value="LOGIN"></td> 
					</tr>
					
				</table>
			</form>
				<?php
					if(isset($_POST['kirim'])) {
						$nm = $_POST['nma'];
						$pwa = $_POST['pw'];
						if(($nm == "nimanda") && ($pwa == "vokasijaya")){
								echo "LOGIN BERHASIL"; 
						}else{
							echo "LOGIN GAGAL";
						}
					}
				
				?>
			</div>
			<div class="card-footer">
			  <small class="text-muted">Login ya</small>
			</div>
		  </div>
		</div>
		
		<img src="h.png" class="d-block w-100" alt="...">
		
		
	</body>
</html>